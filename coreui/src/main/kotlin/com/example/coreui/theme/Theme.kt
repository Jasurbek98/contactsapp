package com.example.coreui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Typography
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.Immutable
import androidx.compose.runtime.staticCompositionLocalOf
import androidx.compose.ui.graphics.Color

@Immutable
data class ContactAppColors(
    val statusBar: Color,
    val secondaryStatusBar: Color,
    val primaryAppbar: Color,
    val SecondaryAppbar: Color,

    val primaryBackground: Color,
    val secondaryBackground: Color,
    val primaryButtonBackground: Color,
    val disabledButtonBackground: Color,
    val secondaryButtonBackground: Color,
    val inactiveButtonBackground: Color,
    val contactPlaceHolderBackground: Color,
    val disabled: Color,
    val stopButtonBackground: Color,

    val primaryText: Color,
    val positiveText: Color,
    val titleText: Color,
    val primaryClickableText: Color,
    val secondaryText: Color,
    val inactiveText: Color,
    val hintText: Color,
    val primaryButtonText: Color,
    val clearPrimaryButtonText: Color,
    val secondaryButtonText: Color,
    val testResultText: Color,
    val advertisingText: Color,
    val errorText: Color,
    val approvedStatusText: Color,
    val pendingStatusText: Color,
    val rejectedStatusText: Color,

    val primaryDivider: Color,
    val transparent60: Color,
    val transparentResult: Color,
    val transparent: Color,
    val nonActionText: Color,
    val primaryInputBackground: Color,
    val secondaryInputBackground: Color,
    val mapInputBackground: Color,
    val selectedFileBackground: Color,
    val badgeAvatarBackground: Color,
    val defaultPlasticCardBackground: Color,
    val plasticCardTitleText: Color,
    val plasticCardNumberText: Color,
    val plasticCardDateText: Color,
    val plasticCardCVCText: Color,
    val stripeTitleText: Color,
    val homeTopBarBackground: Color,
    val homeTopBarCenterGradient: Color,
    val homeTopBarCenter2Gradient: Color,
    val homeTopBarBottomGradient: Color,
    val sidebarBackground: Color,
    val orderItemBackground: Color,
    val acceptedDriverItemBackground: Color,
    val switchThumbColor: Color,
    val checkedTrackColor: Color,
    val unCheckedTrackColor: Color,
    val cardItemInactiveTitleText: Color,
    val supportBodyBackground: Color,
    val labeledButtonBorderTint: Color,
    val secondaryBodyBackground: Color,
    val lightBackground: Color,
    val videoFrameBackground: Color,
    val bottomSheetBackground: Color,
    val bottomSheetSecondaryBackground: Color,
    val bottomSheetDivider: Color,
    val variantButtonBackground: Color,
    val inactiveBorder: Color,
    val activeBorder: Color,
    val mapPlaceListBackground: Color,
    val secondaryDivider: Color,
    val mapRouteLineTint: Color,
    val driverWaveTint: Color,
    val riderWaveTint: Color,
    val recentLocationItemBackground: Color,
    val orderTimeText: Color,
    val durationLineTint: Color,
    val durationLineSecondTint: Color,
    val arrivedSignalBackground: Color,
    val overlayFoggyTransparentTint: Color,
    val finishSwipeButtonBackground: Color,
    val chatBackGround: Color,
    val chatTextFieldBackGround: Color,
    val chatSentMessageTextColor: Color,
    val chatSentMessageBackground: Color,
    val chatReceivedMessageBackground: Color,
    val chatReceivedMessageTextColor: Color,
    val chatSentMessageTimeColor: Color,
    val chatReceivedMessageTimeColor: Color,
    val chatUnRideMessagesCountBackground: Color,
    val disabledMessageBackground: Color,
    val disableText: Color,
)

@Composable
fun PrimaryAppTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    colors: ContactAppColors = ThemeColor.getColors(darkTheme),
    content: @Composable () -> Unit
) {
    CompositionLocalProvider(
        LocalColors provides colors
    ) {
        MaterialTheme(content = content)
    }
}

val LocalColors = staticCompositionLocalOf<ContactAppColors> {
    error("No LocalColors specified")
}

object ContactAppTheme {
    val colors: ContactAppColors
        @Composable
        get() = LocalColors.current

    val typography: Typography
        @Composable
        get() = MaterialTheme.typography
}
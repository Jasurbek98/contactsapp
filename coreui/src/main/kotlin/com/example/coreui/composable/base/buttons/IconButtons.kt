package com.example.coreui.composable.base.buttons

import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.painterResource


@Composable
fun AppIconButton(resourceId: Int, onClick: () -> Unit) {
    IconButton(onClick = onClick) {
        Icon(
            painter = painterResource(id = resourceId),
            contentDescription = null
        )
    }
}

package com.example.coreui.composable.base.textfields

import android.text.TextUtils
import android.util.Patterns
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.coreui.composable.base.Spacer15dp
import com.example.coreui.composable.base.textfields.transformations.PhoneFormatTransformation
import com.example.coreui.composable.base.textfields.transformations.PrefixTransformation
import com.example.coreui.theme.ContactAppTheme
import com.example.coreui.composable.base.texts.ErrorText12sp
import com.example.coreui.composable.base.texts.Text16Hint
import java.util.regex.Pattern

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AppLabeledTextField(
    textState: MutableState<String>,
    hintText: String,
    defMarginTop: Boolean = true,
) {
    Column {
        if (defMarginTop) Spacer15dp()
        TextField(
            modifier = Modifier
                .fillMaxWidth()
                .clip(RoundedCornerShape(12.dp)),
            value = textState.value,
            colors = TextFieldDefaults.textFieldColors(
                containerColor = ContactAppTheme.colors.primaryInputBackground,
                cursorColor = Color.Black,
                disabledLabelColor = Color.Transparent,
                focusedIndicatorColor = ContactAppTheme.colors.transparent,
                unfocusedIndicatorColor = ContactAppTheme.colors.transparent
            ),
            maxLines = 1,
            singleLine = true,
            textStyle = TextStyle(
                color = ContactAppTheme.colors.primaryText,
                fontSize = 16.sp,
                fontWeight = FontWeight.Bold
            ),
            onValueChange = { textState.value = it },
            label = {
                Text16Hint(
                    hintText,
                    color = ContactAppTheme.colors.inactiveText
                )
            }
        )
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AppLabeledEmailTextField(
    textState: MutableState<String>,
    validationState: MutableState<Boolean>,
    backGroundColor: Color = ContactAppTheme.colors.primaryInputBackground,
    defMarginTop: Boolean = true,
) {

    val showErrorState = remember {
        mutableStateOf(false)
    }

    Column {
        if (defMarginTop) Spacer15dp()
        TextField(
            modifier = Modifier
                .fillMaxWidth()
                .clip(RoundedCornerShape(12.dp)),
            value = textState.value,
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Email),
            colors = TextFieldDefaults.textFieldColors(
                containerColor = backGroundColor,
                cursorColor = Color.Black,
                disabledLabelColor = Color.Transparent,
                focusedIndicatorColor = ContactAppTheme.colors.transparent,
                unfocusedIndicatorColor = ContactAppTheme.colors.transparent
            ),
            maxLines = 1,
            singleLine = true,
            textStyle = TextStyle(
                color = ContactAppTheme.colors.primaryText,
                fontSize = 16.sp,
                fontWeight = FontWeight.Bold
            ),
            onValueChange = {
                textState.value = it
                validationState.value = isValidEmail(it)
                showErrorState.value = textState.value.isNotEmpty() && !validationState.value

            },
            label = {
                Text16Hint(
                    "E-mail",
                    color = if (showErrorState.value) ContactAppTheme.colors.errorText
                    else ContactAppTheme.colors.inactiveText
                )
            }
        )

        if (showErrorState.value) {
            ErrorText12sp(text = "Wrong email format*(example: example@example.com)")
        }
    }
}

fun isValidEmail(email: String): Boolean {
    return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AppPhoneCreateTextField(
    textState: MutableState<String>,
    validationState: MutableState<Boolean>,
    backGroundColor: Color = ContactAppTheme.colors.primaryInputBackground,
    defMarginTop: Boolean = true,
) {

    val showErrorState = remember {
        mutableStateOf(false)
    }

    Column {
        if (defMarginTop) Spacer15dp()
        TextField(
            modifier = Modifier
                .fillMaxWidth()
                .clip(RoundedCornerShape(12.dp)),
            value = textState.value,
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Phone),
            visualTransformation = PhoneFormatTransformation(),
            colors = TextFieldDefaults.textFieldColors(
                containerColor = backGroundColor,
                cursorColor = Color.Black,
                disabledLabelColor = Color.Transparent,
                focusedIndicatorColor = ContactAppTheme.colors.transparent,
                unfocusedIndicatorColor = ContactAppTheme.colors.transparent
            ),
            maxLines = 1,
            singleLine = true,
            textStyle = TextStyle(
                color = ContactAppTheme.colors.primaryText,
                fontSize = 16.sp,
                fontWeight = FontWeight.Bold
            ),
            onValueChange = {
                if (it.length <= 11 && Pattern.matches("^[0-9]*$", it)) {
                    textState.value = it
                    validationState.value = it.length == 11
                    showErrorState.value = textState.value.isNotEmpty() && !validationState.value
                }
            },
            label = {
                Text16Hint(
                    "",
                    color = if (showErrorState.value) ContactAppTheme.colors.errorText
                    else ContactAppTheme.colors.inactiveText
                )
            }
        )

        if (showErrorState.value) {
            ErrorText12sp(text = "Wrong phone number format!")
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AppMaskedPhoneTextField(
    textState: MutableState<String>,
    backGroundColor: Color = ContactAppTheme.colors.primaryInputBackground,
    defMarginTop: Boolean = true,
) {

    Column {
        if (defMarginTop) Spacer15dp()
        TextField(
            modifier = Modifier
                .fillMaxWidth()
                .clip(RoundedCornerShape(12.dp)),
            value = textState.value,
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Phone),
            visualTransformation = PhoneFormatTransformation(),
            colors = TextFieldDefaults.textFieldColors(
                containerColor = backGroundColor,
                cursorColor = Color.Black,
                disabledLabelColor = Color.Transparent,
                focusedIndicatorColor = ContactAppTheme.colors.transparent,
                unfocusedIndicatorColor = ContactAppTheme.colors.transparent
            ),
            maxLines = 1,
            singleLine = true,
            textStyle = TextStyle(
                color = ContactAppTheme.colors.primaryText,
                fontSize = 16.sp,
                fontWeight = FontWeight.Bold
            ),
            onValueChange = {
                if (it.length <= 11 && Pattern.matches("^[0-9]*$", it)) {
                    textState.value = it
                }
            },
            label = {
                Text16Hint(
                    "Phone",
                    color = ContactAppTheme.colors.inactiveText
                )
            }
        )
    }
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AppLabeledPhoneTextField(
    textState: MutableState<String>,
    hintText: String,
    defMarginTop: Boolean = true,
) {

    Column {
        if (defMarginTop) Spacer15dp()
        TextField(
            modifier = Modifier
                .fillMaxWidth()
                .clip(RoundedCornerShape(12.dp)),
            value = textState.value,
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Phone),
            visualTransformation = PrefixTransformation("+"),
            colors = TextFieldDefaults.textFieldColors(
                containerColor = ContactAppTheme.colors.primaryInputBackground,
                cursorColor = Color.Black,
                disabledLabelColor = Color.Transparent,
                focusedIndicatorColor = ContactAppTheme.colors.transparent,
                unfocusedIndicatorColor = ContactAppTheme.colors.transparent
            ),
            maxLines = 1,
            singleLine = true,
            textStyle = TextStyle(
                color = ContactAppTheme.colors.primaryText,
                fontSize = 16.sp,
                fontWeight = FontWeight.Bold
            ),
            onValueChange = {
                textState.value = it
            },
            label = {
                Text16Hint(
                    hintText,
                    color = ContactAppTheme.colors.inactiveText
                )
            }
        )
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AppLabeledBigTextField(
    textState: MutableState<String>,
    hintText: String,
    defMarginTop: Boolean = true,
) {
    Column {
        if (defMarginTop) Spacer15dp()
        TextField(
            modifier = Modifier
                .fillMaxWidth()
                .defaultMinSize(minHeight = 120.dp)
                .requiredHeight(160.dp)
                .clip(RoundedCornerShape(12.dp)),
            value = textState.value,
            colors = TextFieldDefaults.textFieldColors(
                containerColor = ContactAppTheme.colors.primaryInputBackground,
                cursorColor = Color.Black,
                disabledLabelColor = Color.Transparent,
                focusedIndicatorColor = ContactAppTheme.colors.transparent,
                unfocusedIndicatorColor = ContactAppTheme.colors.transparent
            ),
            textStyle = TextStyle(
                color = ContactAppTheme.colors.primaryText,
                fontSize = 16.sp,
                fontWeight = FontWeight.Bold
            ),
            onValueChange = { textState.value = it },
            label = {
                Text16Hint(
                    hintText,
                    color = ContactAppTheme.colors.inactiveText
                )
            }
        )
    }
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AppCommentTextField(
    textState: MutableState<String>,
    hintText: String,
    defMarginTop: Boolean = true,
) {
    Column {
        if (defMarginTop) Spacer15dp()
        TextField(
            maxLines = 3,
            modifier = Modifier
                .fillMaxWidth()
                .clip(RoundedCornerShape(12.dp)),
            value = textState.value,
            colors = TextFieldDefaults.textFieldColors(
                containerColor = ContactAppTheme.colors.secondaryInputBackground,
                cursorColor = Color.Black,
                disabledLabelColor = Color.Transparent,
                focusedIndicatorColor = ContactAppTheme.colors.transparent,
                unfocusedIndicatorColor = ContactAppTheme.colors.transparent
            ),
            textStyle = TextStyle(
                color = ContactAppTheme.colors.primaryText,
                fontSize = 16.sp,
                fontWeight = FontWeight.Bold
            ),
            onValueChange = { textState.value = it },
            placeholder = { Text16Hint(text = hintText) }
        )
    }
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AppLabeledPasswordTextField(
    textState: MutableState<String>,
    hintText: String,
    defMarginTop: Boolean = true,
    activeIconResourceId: Int,
    inactiveIconResourceId: Int
) {

    var passwordVisibility by remember {
        mutableStateOf(false)
    }

    Column {
        if (defMarginTop) Spacer15dp()
        TextField(
            modifier = Modifier
                .fillMaxWidth()
                .clip(RoundedCornerShape(12.dp)),
            value = textState.value,
            visualTransformation = if (passwordVisibility) VisualTransformation.None else PasswordVisualTransformation(),
            colors = TextFieldDefaults.textFieldColors(
                containerColor = ContactAppTheme.colors.primaryInputBackground,
                cursorColor = Color.Black,
                disabledLabelColor = Color.Transparent,
                focusedIndicatorColor = ContactAppTheme.colors.transparent,
                unfocusedIndicatorColor = ContactAppTheme.colors.transparent
            ),
            textStyle = TextStyle(
                color = ContactAppTheme.colors.primaryText,
                fontSize = 16.sp,
                fontWeight = FontWeight.Bold
            ),
            onValueChange = {
                textState.value = it
            },
            label = {
                Text16Hint(
                    hintText,
                    color = ContactAppTheme.colors.inactiveText
                )
            },
            trailingIcon = {
                IconButton(onClick = { passwordVisibility = !passwordVisibility }) {
                    Icon(
                        painter = painterResource(id = if (passwordVisibility) activeIconResourceId else inactiveIconResourceId),
                        contentDescription = null
                    )
                }
            }
        )
    }
}

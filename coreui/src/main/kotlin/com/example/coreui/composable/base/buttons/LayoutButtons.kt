package com.example.coreui.composable.base.buttons

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.coreui.theme.ContactAppTheme
import com.example.coreui.composable.base.Spacer1dp

@Composable
fun LabeledTextWithIconButton(
    labelText: String,
    iconResourceId: Int, valueText: String, onClick: () -> Unit
) {
    Column(Modifier.fillMaxWidth()) {
        Row(
            Modifier
                .fillMaxWidth()
                .clickable { onClick() }
                .padding(vertical = 15.dp, horizontal = 24.dp),
            verticalAlignment = Alignment.CenterVertically) {
            Text(
                fontSize = 16.sp,
                text = labelText,
                maxLines = 1,
                color = ContactAppTheme.colors.primaryText,
                modifier = Modifier.weight(1f)
            )
            Text(
                fontSize = 16.sp,
                text = valueText,
                maxLines = 1,
                color = ContactAppTheme.colors.inactiveText,
                overflow = TextOverflow.Ellipsis,
                modifier = Modifier.weight(1f)
            )
            Image(
                painter = painterResource(iconResourceId),
                contentDescription = "",
                Modifier
                    .size(24.dp)
            )
        }
        Spacer1dp()
    }
}

@Composable
fun LabeledDateTextWithIconButton(labelText: String, iconResourceId: Int, valueText: String, onClick: () -> Unit) {
    Column(Modifier.fillMaxWidth()) {
        Row(
            Modifier
                .fillMaxWidth()
                .clickable { onClick() }
                .padding(vertical = 15.dp, horizontal = 24.dp),
            verticalAlignment = Alignment.CenterVertically) {
            Text(
                fontSize = 16.sp,
                text = labelText,
                maxLines = 1,
                color = ContactAppTheme.colors.primaryText,
                modifier = Modifier.weight(1f)
            )
            Text(
                fontSize = 16.sp,
                text = valueText,
                maxLines = 1,
                color = ContactAppTheme.colors.inactiveText,
                overflow = TextOverflow.Ellipsis,
                modifier = Modifier.weight(1f)
            )
            Image(
                painter = painterResource(iconResourceId),
                contentDescription = "",
                Modifier
                    .size(24.dp)
            )
        }
        Spacer1dp()
    }
}






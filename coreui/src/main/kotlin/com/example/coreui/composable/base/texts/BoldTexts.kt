package com.example.coreui.composable.base.texts

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType.Companion.Text
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.sp
import com.example.coreui.theme.ContactAppTheme

@Composable
fun Text24spBoldSecond(text: String, color: Color = ContactAppTheme.colors.secondaryText) {
    Text(
        text = text,
        fontSize = 24.sp,
        fontWeight = FontWeight.Bold,
        color = color
    )
}


@Composable
fun Text24spBoldTitle(text: String, color: Color = ContactAppTheme.colors.primaryText) {
    Text(
        text = text,
        fontSize = 24.sp,
        fontWeight = FontWeight.Bold,
        color = color,
    )
}


@Composable
fun Text32spBoldSecond(text: String, color: Color = ContactAppTheme.colors.secondaryText) {
    Text(
        text = text,
        fontSize = 24.sp,
        fontWeight = FontWeight.Bold,
        color = color
    )
}

@Composable
fun Text32spBoldTitle(text: String, color: Color = ContactAppTheme.colors.titleText) {
    Text(
        text = text,
        fontSize = 32.sp,
        fontWeight = FontWeight.Bold,
        color = color
    )
}


@Composable
fun Text12spBoldTitle(text: String, color: Color = ContactAppTheme.colors.primaryText) {
    Text(
        text = text,
        fontSize = 12.sp,
        fontWeight = FontWeight.Bold,
        color = color
    )
}

@Composable
fun Text14spBoldTitle(text: String, color: Color = ContactAppTheme.colors.primaryText) {
    Text(
        text = text,
        fontSize = 14.sp,
        fontWeight = FontWeight.Bold,
        color = color
    )
}

@Composable
fun Text16spHint(text: String, color: Color = ContactAppTheme.colors.inactiveText) {
    Text(
        text = text,
        fontSize = 16.sp,
        color = color
    )
}

@Composable
fun Text16spBoldTitle(
    text: String,
    color: Color = ContactAppTheme.colors.primaryText,
    alignCenter: Boolean = true
) {
    Text(
        text = text,
        fontSize = 16.sp,
        fontWeight = FontWeight.Bold,
        color = color,
        textAlign = if (alignCenter) TextAlign.Center else TextAlign.Start,
    )
}


@Composable
fun Text18spBoldTitle(
    text: String,
    color: Color = ContactAppTheme.colors.primaryText,
    alignCenter: Boolean = true
) {
    Text(
        text = text,
        fontSize = 18.sp,
        fontWeight = FontWeight.Bold,
        color = color,
        textAlign = if (alignCenter) TextAlign.Center else TextAlign.Start,
    )
}


@Composable
fun Text16spBoldTitleEllipsize(
    text: String,
    color: Color = ContactAppTheme.colors.primaryText,
    alignCenter: Boolean = true
) {
    Text(
        text = text,
        fontSize = 16.sp,
        fontWeight = FontWeight.Bold,
        color = color,
        maxLines = 1,
        textAlign = if (alignCenter) TextAlign.Center else TextAlign.Start,
        overflow = TextOverflow.Ellipsis
    )
}


@Composable
fun Text16spBoldAlignCenter(text: String, color: Color = ContactAppTheme.colors.primaryText) {
    Text(
        text = text,
        fontSize = 16.sp,
        fontWeight = FontWeight.Bold,
        color = color,
        textAlign = TextAlign.Center,
    )
}


@Composable
fun Text16spBoldActiveTitle(text: String, color: Color = ContactAppTheme.colors.titleText) {
    Text(
        text = text,
        fontSize = 16.sp,
        fontWeight = FontWeight.Bold,
        color = color
    )
}


@Composable
fun Text14spBoldTextCenterTitle(text: String, color: Color = ContactAppTheme.colors.primaryText) {
    Text(
        text = text,
        fontSize = 14.sp,
        fontWeight = FontWeight.Bold,
        color = color,
        textAlign = TextAlign.Center
    )
}

@Composable
fun Text14spBoldActiveTextCenter(
    text: String,
    color: Color = ContactAppTheme.colors.primaryClickableText
) {
    Text(
        text = text,
        fontSize = 14.sp,
        fontWeight = FontWeight.Bold,
        color = color,
        textAlign = TextAlign.Center
    )
}


@Composable
fun Text32spBold(text: String, color: Color = ContactAppTheme.colors.primaryText) {
    Text(
        text = text,
        fontSize = 32.sp,
        fontWeight = FontWeight.Bold,
        color = color
    )
}


@Composable
fun Text20spBoldPositive(text: String, color: Color = ContactAppTheme.colors.positiveText) {
    Text(
        text = text,
        fontSize = 20.sp,
        fontWeight = FontWeight.Bold,
        color = color,
    )
}


@Composable
fun Text24spTestResult(text: String, color: Color = ContactAppTheme.colors.testResultText) {
    Text(
        text = text,
        fontSize = 24.sp,
        fontWeight = FontWeight.Bold,
        color = color
    )
}


@Composable
fun Text48spTestResult(text: String, color: Color = ContactAppTheme.colors.testResultText) {
    Text(
        text = text,
        fontSize = 48.sp,
        fontWeight = FontWeight.Bold,
        color = color
    )
}

@Composable
fun Text20spBoldTitle(
    text: String,
    color: Color = ContactAppTheme.colors.primaryText,
    alignCenter: Boolean = false
) {
    Text(
        text = text,
        fontSize = 20.sp,
        fontWeight = FontWeight.Bold,
        color = color,
        textAlign = if (alignCenter) TextAlign.Center else TextAlign.Start,
    )
}


@Composable
fun Text16spBoldValue(text: String, color: Color = ContactAppTheme.colors.primaryText) {
    Text(
        text = text,
        fontSize = 16.sp,
        fontWeight = FontWeight.Bold,
        color = color,
        maxLines = 1,
        overflow = TextOverflow.Ellipsis
    )
}



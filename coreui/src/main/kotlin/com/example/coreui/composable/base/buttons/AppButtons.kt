package com.example.coreui.composable.base.buttons

import android.widget.Button
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.coreui.theme.ContactAppTheme


@Composable
fun ButtonPrimaryRounded(
    modifier: Modifier = Modifier,
    text: String = "",
    textColor: Color = ContactAppTheme.colors.primaryButtonText,
    enabled: Boolean = true,
    btnBackgroundColor: Color = ContactAppTheme.colors.primaryButtonBackground,
    onClick: () -> Unit,
) {
    Button(
        onClick = onClick,
        enabled = enabled,
        elevation = null,
        colors = ButtonDefaults.buttonColors(
            containerColor = btnBackgroundColor,
            disabledContainerColor = ContactAppTheme.colors.disabledButtonBackground
        ),
        shape = RoundedCornerShape(12.dp),
        modifier = modifier
            .fillMaxWidth()

    ) {
        Text(
            text = text,
            fontSize = 16.sp,
            fontWeight = FontWeight.Bold,
            color = textColor,
            modifier = Modifier.padding(8.dp)
        )
    }
}


@Composable
fun ButtonPrimaryRoundedWithMargin(
    modifier: Modifier = Modifier,
    text: String = "",
    horizontalMargin: Dp = 24.dp,
    verticalMargin: Dp = 0.dp,
    textColor: Color = ContactAppTheme.colors.primaryButtonText,
    enabled: Boolean = true,
    btnBackgroundColor: Color = ContactAppTheme.colors.primaryButtonBackground,
    onClick: () -> Unit,
) {
    Button(
        onClick = onClick,
        enabled = enabled,
        elevation = null,
        colors = ButtonDefaults.buttonColors(
            containerColor = btnBackgroundColor,
            disabledContainerColor = ContactAppTheme.colors.disabledButtonBackground
        ),
        shape = RoundedCornerShape(12.dp),
        modifier = modifier
            .padding(horizontal = horizontalMargin, vertical = verticalMargin)
            .fillMaxWidth()

    ) {
        Text(
            text = text,
            fontSize = 16.sp,
            fontWeight = FontWeight.Bold,
            color = textColor,
            modifier = Modifier.padding(8.dp)
        )
    }
}

@Composable
fun ButtonSecondaryRounded(
    modifier: Modifier = Modifier,
    text: String = "",
    textColor: Color = ContactAppTheme.colors.primaryButtonText,
    enabled: Boolean = true,
    btnBackgroundColor: Color = ContactAppTheme.colors.variantButtonBackground,
    onClick: () -> Unit,
) {
    Button(
        onClick = onClick,
        enabled = enabled,
        elevation = null,
        colors = ButtonDefaults.buttonColors(containerColor = btnBackgroundColor),
        shape = RoundedCornerShape(12.dp),
        modifier = modifier
            .fillMaxWidth()

    ) {
        Text(
            text = text,
            fontSize = 16.sp,
            fontWeight = FontWeight.Bold,
            color = textColor,
            modifier = Modifier.padding(8.dp)
        )
    }
}


@Composable
fun RowScope.ButtonSecondaryWeightRounded(
    modifier: Modifier = Modifier,
    text: String = "",
    textColor: Color = ContactAppTheme.colors.primaryButtonText,
    enabled: Boolean = true,
    weight: Float = 1f,
    btnBackgroundColor: Color = ContactAppTheme.colors.variantButtonBackground,
    onClick: () -> Unit,
) {
    Button(
        onClick = onClick,
        enabled = enabled,
        elevation = null,
        colors = ButtonDefaults.buttonColors(containerColor = btnBackgroundColor),
        shape = RoundedCornerShape(12.dp),
        modifier = modifier.weight(weight)
    ) {
        Text(
            text = text,
            fontSize = 16.sp,
            fontWeight = FontWeight.Bold,
            color = textColor,
            modifier = Modifier.padding(8.dp)
        )
    }
}


@Composable
fun ButtonSecondaryWeightRounded(
    modifier: Modifier = Modifier,
    text: String = "",
    textColor: Color = ContactAppTheme.colors.primaryButtonText,
    enabled: Boolean = true,
    btnBackgroundColor: Color = ContactAppTheme.colors.variantButtonBackground,
    onClick: () -> Unit,
) {
    Button(
        onClick = onClick,
        enabled = enabled,
        elevation = null,
        colors = ButtonDefaults.buttonColors(containerColor = btnBackgroundColor),
        shape = RoundedCornerShape(12.dp),
        modifier = modifier.fillMaxWidth()
    ) {
        Text(
            text = text,
            fontSize = 16.sp,
            fontWeight = FontWeight.Bold,
            color = textColor,
            modifier = Modifier.padding(8.dp)
        )
    }
}

@Composable
fun RowScope.ButtonPrimaryWeightRounded(
    modifier: Modifier = Modifier,
    text: String = "",
    textColor: Color = ContactAppTheme.colors.primaryButtonText,
    enabled: Boolean = true,
    weight: Float = 1f,
    btnBackgroundColor: Color = ContactAppTheme.colors.primaryButtonBackground,
    onClick: () -> Unit,
) {
    Button(
        onClick = onClick,
        enabled = enabled,
        elevation = null,
        colors = ButtonDefaults.buttonColors(
            containerColor = btnBackgroundColor,
            disabledContainerColor = ContactAppTheme.colors.disabledButtonBackground
        ),
        shape = RoundedCornerShape(12.dp),
        modifier = modifier.weight(weight)
    ) {
        Text(
            text = text,
            fontSize = 16.sp,
            fontWeight = FontWeight.Bold,
            color = textColor,
            modifier = Modifier.padding(8.dp)
        )
    }
}


@Composable
fun ButtonSecondaryRoundedWithBorder(
    modifier: Modifier = Modifier,
    text: String = "",
    textColor: Color = ContactAppTheme.colors.secondaryButtonText,
    enabled: Boolean = true,
    btnBackgroundColor: Color = ContactAppTheme.colors.transparent,
    onClick: () -> Unit,
) {
    Button(
        onClick = onClick,
        enabled = enabled,
        colors = ButtonDefaults.buttonColors(containerColor = btnBackgroundColor),
        elevation = null,
        shape = RoundedCornerShape(12.dp),
        border = BorderStroke(2.dp, color = ContactAppTheme.colors.secondaryButtonText),
        modifier = modifier
            .fillMaxWidth()
            .background(btnBackgroundColor)

    ) {
        Text(
            text = text,
            fontSize = 16.sp,
            color = textColor,
            fontWeight = FontWeight.Bold,
            modifier = Modifier.padding(8.dp)
        )
    }
}


@Composable
fun RowScope.PositiveButton(
    modifier: Modifier = Modifier,
    text: String = "",
    textColor: Color = ContactAppTheme.colors.primaryButtonText,
    enabled: Boolean = true,
    weight: Float = 1f,
    btnBackgroundColor: Color = ContactAppTheme.colors.primaryButtonBackground,
    onClick: () -> Unit,
) {
    Button(
        onClick = onClick,
        enabled = enabled,
        elevation = null,
        colors = ButtonDefaults.buttonColors(containerColor = btnBackgroundColor),
        shape = RoundedCornerShape(12.dp),
        modifier = modifier.weight(weight)
    ) {
        Text(
            text = text,
            fontSize = 16.sp,
            fontWeight = FontWeight.Bold,
            color = textColor,
            modifier = Modifier.padding(2.dp)
        )
    }
}


@Composable
fun RowScope.NegativeButton(
    modifier: Modifier = Modifier,
    text: String = "",
    textColor: Color = ContactAppTheme.colors.primaryButtonText,
    enabled: Boolean = true,
    weight: Float = 1f,
    btnBackgroundColor: Color = ContactAppTheme.colors.variantButtonBackground,
    onClick: () -> Unit,
) {
    Button(
        onClick = onClick,
        enabled = enabled,
        elevation = null,
        colors = ButtonDefaults.buttonColors(containerColor = btnBackgroundColor),
        shape = RoundedCornerShape(12.dp),
        modifier = modifier.weight(weight)
    ) {
        Text(
            text = text,
            fontSize = 16.sp,
            fontWeight = FontWeight.Bold,
            color = textColor,
            modifier = Modifier.padding(2.dp)
        )
    }
}


@Composable
fun ClearPrimaryButton(
    text: String = "",
    textColor: Color = ContactAppTheme.colors.clearPrimaryButtonText,
    enabled: Boolean = true,
    btnBackgroundColor: Color = ContactAppTheme.colors.variantButtonBackground,
    onClick: () -> Unit,
) {
    Button(
        onClick = onClick,
        enabled = enabled,
        elevation = null,
        colors = ButtonDefaults.buttonColors(containerColor = btnBackgroundColor),
        shape = RoundedCornerShape(12.dp),
    ) {
        Text(
            text = text,
            fontSize = 16.sp,
            fontWeight = FontWeight.Bold,
            color = textColor,
            modifier = Modifier.padding(2.dp)
        )
    }
}

package com.example.contactapp.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Created by Jasurbek Kurganbayev 31/07/2023
 */
@HiltAndroidApp
class App : Application() {

    override fun onCreate() {
        instance = this
        super.onCreate()
    }

    companion object {
        lateinit var instance: App
    }
}
package com.example.contactapp.di

import android.content.Context
import com.example.core.database.ContactDatabase
import com.example.core.database.dao.ContactDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * Created by Jasurbek Kurganbayev 31/07/2023
 */
@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {

    @Provides
    @Singleton
    fun getDatabase(@ApplicationContext context: Context): ContactDatabase =
        ContactDatabase.getDatabase(context)

    @Provides
    @Singleton
    fun getContactDao(appDatabase: ContactDatabase): ContactDao = appDatabase.contactDao()
}
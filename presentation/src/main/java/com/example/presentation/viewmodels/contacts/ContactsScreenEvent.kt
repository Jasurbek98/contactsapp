package com.example.presentation.viewmodels.contacts

import androidx.compose.runtime.Immutable
import com.example.core.database.entity.Contact
import com.example.presentation.mvibase.UiEvent

/**
 * Created by Jasurbek Kurganbayev 01/08/2023
 */
@Immutable
sealed class ContactsScreenEvent : UiEvent {

    data class ShowData(val items: List<Contact>) : ContactsScreenEvent()
    data class Error(val throwable: Throwable) : ContactsScreenEvent()
    data class NavigateTo(val route: String) : ContactsScreenEvent()

    object DeleteAll : ContactsScreenEvent()
}
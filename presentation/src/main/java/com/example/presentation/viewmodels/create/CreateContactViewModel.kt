package com.example.presentation.viewmodels.create

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.viewModelScope
import com.example.core.data.AppResponse
import com.example.core.repository.ContactRepository
import com.example.presentation.mvibase.BaseViewModel
import com.example.presentation.mvibase.Reducer
import com.example.presentation.util.ContactUiState
import com.example.presentation.util.toContact
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by Jasurbek Kurganbayev 01/08/2023
 */
@HiltViewModel
class CreateContactViewModel @Inject constructor(
    private val repository: ContactRepository,
) : BaseViewModel<com.example.presentation.viewmodels.details.ContactDetailsScreenState, CreateContactScreenEvent, CreateContactScreenSideEffect>() {

    private val reducer = MainReducer(CreateContactScreenState.initial())

    val contactUiState = mutableStateOf(ContactUiState())

    override val state: StateFlow<com.example.presentation.viewmodels.details.ContactDetailsScreenState> = reducer.state
    override val sideEffect: Flow<CreateContactScreenSideEffect> = reducer.sideEffect

    fun updateUiState(contactUiState: ContactUiState) {
        this.contactUiState.value = contactUiState
        this.contactUiState.value =
            contactUiState.copy(actionEnable = contactUiState.fullName.isNotEmpty() && contactUiState.mobile?.isNotEmpty() == true)
    }


    fun createContact() = viewModelScope.launch(Dispatchers.IO) {
        sendEvent(CreateContactScreenEvent.CreateSend)
        when (val resp = repository.addContact(
            contactUiState.value.toContact()
        )) {
            is AppResponse.Error -> {
                CreateContactScreenEvent.Error(throwable = resp.throwable)
            }

            is AppResponse.Success -> {
                CreateContactScreenEvent.CreateSend
            }
        }
    }

    fun navigateUp() {
        sendEvent(CreateContactScreenEvent.NavigateUp)
    }

    private fun sendEvent(event: CreateContactScreenEvent) {
        reducer.sendEvent(event)
    }

    private class MainReducer(initial: com.example.presentation.viewmodels.details.ContactDetailsScreenState) :
        Reducer<com.example.presentation.viewmodels.details.ContactDetailsScreenState, CreateContactScreenEvent, CreateContactScreenSideEffect>(
            initial
        ) {
        override fun reduce(oldState: com.example.presentation.viewmodels.details.ContactDetailsScreenState, event: CreateContactScreenEvent) {
            when (event) {
                is CreateContactScreenEvent.CreateSend -> {
                    setState(oldState.copy(isShowProgress = true))
                    setSideEffect(CreateContactScreenSideEffect.Created)
                }

                is CreateContactScreenEvent.Created -> {
                    setState(oldState.copy(isShowProgress = false))
                    setSideEffect(CreateContactScreenSideEffect.Created)
                }

                is CreateContactScreenEvent.Error -> {
                    setState(oldState.copy(isShowProgress = false))
                    setSideEffect(CreateContactScreenSideEffect.ShowError(message = event.throwable.message.toString()))
                }

                CreateContactScreenEvent.NavigateUp -> {
                    setSideEffect(CreateContactScreenSideEffect.NavigateUp)
                }
            }
        }

    }
}


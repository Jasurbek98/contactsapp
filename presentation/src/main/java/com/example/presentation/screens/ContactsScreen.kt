package com.example.presentation.screens

import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.navigationBarsPadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.example.core.database.entity.Contact
import com.example.coreui.R
import com.example.coreui.composable.base.ProgressBarWithBlock
import com.example.coreui.theme.ContactAppTheme
import com.example.coreui.util.CONTACT_DETAILS_SCREEN_ROUTE
import com.example.coreui.util.CREATE_CONTACT_SCREEN_ROUTE
import com.example.presentation.ContactsTopAppBar
import com.example.presentation.destinations.buildContactDetailsRoute
import com.example.presentation.viewmodels.contacts.ContactsScreenSideEffect
import com.example.presentation.viewmodels.contacts.ContactsViewModel
import kotlinx.coroutines.flow.collect

/**
 * Created by Jasurbek Kurganbayev 31/07/2023
 */

@ExperimentalMaterial3Api
@Composable
fun ContactsScreen(
    navController: NavHostController,
    viewModel: ContactsViewModel = hiltViewModel()
) {

    val homeUiState by viewModel.state.collectAsState()

    val context = LocalContext.current

    LaunchedEffect(key1 = Unit, block = {
        viewModel.getAllContacts()
    })

    LaunchedEffect(key1 = Unit, block = {
        viewModel.sideEffect.collect {
            when (it) {
                ContactsScreenSideEffect.DeleteALlMessage -> {
                    Toast.makeText(
                        context,
                        context.getString(R.string.delete_all_message),
                        Toast.LENGTH_SHORT
                    ).show()
                }

                is ContactsScreenSideEffect.NavigateTo -> {
                    navController.navigate(it.route)
                }

                is ContactsScreenSideEffect.ShowError -> {
                    Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
                }
            }
        }

    })


    Scaffold(
        topBar = {
            ContactsTopAppBar(
                title = stringResource(R.string.contacts_screen_title), navigateBack = false
            )
        },
        floatingActionButton = {
            FloatingActionButton(
                onClick = {
                    viewModel.navigateTo(CREATE_CONTACT_SCREEN_ROUTE)
                }, modifier = Modifier.navigationBarsPadding()
            ) {
                Icon(
                    imageVector = Icons.Default.Add,
                    contentDescription = "Entry Contact",
                    tint = MaterialTheme.colorScheme.onPrimary
                )
            }
        },
    ) { innerPadding ->
        HomeBody(
            contactList = homeUiState.data,
            onContactClick = { viewModel.navigateTo(buildContactDetailsRoute(it)) },
            modifier = Modifier
                .padding(innerPadding)
                .background(MaterialTheme.colorScheme.background)
        )
        if (homeUiState.isShowProgress) ProgressBarWithBlock()

    }

}

@Composable
fun HomeBody(
    contactList: List<Contact>, onContactClick: (Int) -> Unit, modifier: Modifier = Modifier
) {
    if (contactList.isEmpty()) {
        Column(
            modifier = modifier
                .fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                painter = painterResource(R.drawable.list_empty),
                contentDescription = null,
                modifier = modifier.size(200.dp)
            )
            Text(
                text = stringResource(R.string.no_contacts),
                textAlign = TextAlign.Center,
                fontSize = 20.sp,
            )
        }

    } else {
        ContactList(
            modifier = modifier,
            contactList = contactList,
            onContactClick = { onContactClick(it.id) })
    }
}

@Composable
fun ContactList(
    contactList: List<Contact>, onContactClick: (Contact) -> Unit, modifier: Modifier = Modifier
) {
    LazyColumn(
        modifier = modifier
    ) {
        items(items = contactList, key = { it.id }) { contact ->
            ContactItems(
                contact = contact, onContactClick = onContactClick
            )
            Divider()
        }
    }
}

@Composable
fun ContactItems(
    contact: Contact, onContactClick: (Contact) -> Unit, modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier
            .fillMaxSize()
            .padding(16.dp),
        verticalArrangement = Arrangement.spacedBy(16.dp)
    ) {
        Row(modifier = modifier
            .fillMaxWidth()
            .clickable { onContactClick(contact) }) {
            Image(
                painter = painterResource(R.drawable.contacts_icon),
                contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .size(50.dp)
                    .clip(CircleShape)
            )
            Spacer(modifier = modifier.width(8.dp))
            Text(
                text = contact.fullName,
                textAlign = TextAlign.Left,
                modifier = modifier.align(Alignment.CenterVertically)
            )
        }
    }
}

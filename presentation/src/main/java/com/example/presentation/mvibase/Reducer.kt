package com.example.presentation.mvibase

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.receiveAsFlow

/**
 * Created by Jasurbek Kurganbayev 01/08/2023
 */
abstract class Reducer<S : UiState, E : UiEvent, SE : UiSideEffect>(initial: S) {

    private val _state: MutableStateFlow<S> = MutableStateFlow(initial)
    val state: StateFlow<S>
        get() = _state

    private val _sideEffect = Channel<SE>()
    val sideEffect: Flow<SE>
        get() = _sideEffect.receiveAsFlow()

    fun sendEvent(event: E) {
        reduce(_state.value, event)
    }

    fun setState(newState: S) {
        _state.tryEmit(newState)
    }

    fun setSideEffect(newSideEffect: SE) {
        _sideEffect.trySend(newSideEffect)
    }

    abstract fun reduce(oldState: S, event: E)

}

interface UiState

interface UiEvent

interface UiSideEffect
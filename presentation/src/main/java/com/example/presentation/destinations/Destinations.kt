package com.example.presentation.destinations

/**
 * Created by Jasurbek Kurganbayev 31/07/2023
 */

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.composable
import com.example.coreui.util.CONTACTS_SCREEN_ROUTE
import com.example.coreui.util.CONTACT_DETAILS_SCREEN_ROUTE
import com.example.coreui.util.CREATE_CONTACT_SCREEN_ROUTE
import com.example.presentation.screens.ContactDetailsScreen
import com.example.presentation.screens.ContactsScreen
import com.example.presentation.screens.CreateContactScreen

@ExperimentalMaterial3Api
fun NavGraphBuilder.contactsDestination(
    navController: NavHostController,
) {
    composable(CONTACTS_SCREEN_ROUTE) {
        ContactsScreen(navController)
    }
}

fun NavGraphBuilder.contactDetailsDestination(
    navController: NavHostController,
) {
    composable("$CONTACT_DETAILS_SCREEN_ROUTE/{id}") {
        val id = it.arguments?.getString("id")
        ContactDetailsScreen(navController, id!!.toInt())
    }
}

fun buildContactDetailsRoute(id: Int) = "$CONTACT_DETAILS_SCREEN_ROUTE/$id"

@ExperimentalMaterial3Api
fun NavGraphBuilder.createContactDestination(
    navController: NavHostController,
) {
    composable(CREATE_CONTACT_SCREEN_ROUTE) {
        CreateContactScreen(navController)
    }
}


//
//@ExperimentalMaterial3Api
//fun NavGraphBuilder.editContactDestination(
//    navController: NavHostController,
//) {
//    composable(EDIT_CONTACT_SCREEN_ROUTE) {
//        EditContactScreen(navController)
//    }
//}
package com.example.presentation

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.rememberNavController
import com.example.coreui.theme.PrimaryAppTheme
import com.example.coreui.util.CONTACTS_SCREEN_ROUTE
import com.example.presentation.destinations.contactDetailsDestination
import com.example.presentation.destinations.contactsDestination
import com.example.presentation.destinations.createContactDestination

/**
 * Created by Jasurbek Kurganbayev 31/07/2023
 */

@ExperimentalMaterial3Api
@Composable
fun ContactMainComposableRoot() {

    val navController: NavHostController = rememberNavController()

    PrimaryAppTheme {
        Surface {
            NavHost(
                navController = navController, startDestination = CONTACTS_SCREEN_ROUTE
            ) {
                contactsDestination(navController)
                createContactDestination(navController)
//                editContactDestination(navController)
                contactDetailsDestination(navController)
            }
        }
    }

}
package com.example.core.data

/**
 * Created by Jasurbek Kurganbayev 01/08/2023
 */
sealed class AppResponse<out T : Any> {
    data class Success<out T : Any>(val data: T) : AppResponse<T>()
    data class Error(val throwable: Throwable) : AppResponse<Nothing>()

}

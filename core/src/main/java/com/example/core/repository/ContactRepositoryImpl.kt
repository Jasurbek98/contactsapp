package com.example.core.repository

import com.example.core.data.AppResponse
import com.example.core.database.dao.ContactDao
import com.example.core.database.entity.Contact
import javax.inject.Inject

/**
 * Created by Jasurbek Kurganbayev 31/07/2023
 */
class ContactRepositoryImpl @Inject constructor(private val contactDao: ContactDao) :
    ContactRepository {
    override suspend fun addContact(contact: Contact) =
        try {
            AppResponse.Success(contactDao.insert(contact))
        } catch (error: Exception) {
            AppResponse.Error(throwable = error)
        }

    override suspend fun deleteContact(contact: Contact) =
        try {
            AppResponse.Success(contactDao.delete(contact))
        } catch (error: Exception) {
            AppResponse.Error(throwable = error)
        }


    override suspend fun editContact(contact: Contact) =
        try {
            AppResponse.Success(contactDao.update(contact))
        } catch (error: Exception) {
            AppResponse.Error(throwable = error)
        }

    override suspend fun getAllContacts() =
        try {
            AppResponse.Success(contactDao.getAllContact())
        } catch (error: Exception) {
            AppResponse.Error(throwable = error)
        }

    override suspend fun getContact(id: Int) =
        try {
            AppResponse.Success(contactDao.getContact(id))
        } catch (error: Exception) {
            AppResponse.Error(throwable = error)
        }


}
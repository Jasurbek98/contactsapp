package com.example.core.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.core.database.converters.ListToIntegerConverters
import com.example.core.database.converters.ListToJsonConverters
import com.example.core.database.dao.ContactDao
import com.example.core.database.entity.Contact

/**
 * Created by Jasurbek Kurganbayev 31/07/2023
 */
@Database(entities = [Contact::class], version = 1, exportSchema = true)
@TypeConverters(ListToJsonConverters::class, ListToIntegerConverters::class)
abstract class ContactDatabase : RoomDatabase() {

    abstract fun contactDao(): ContactDao

    companion object {
        @Volatile
        private var INSTANCE: ContactDatabase? = null


        fun getDatabase(context: Context): ContactDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context, ContactDatabase::class.java, "contact_database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}

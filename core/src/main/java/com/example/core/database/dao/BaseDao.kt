package com.example.core.database.dao

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update

/**
 * Created by Davronbek Raximjanov on 16.07.2021
 **/

interface BaseDao<T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(data: T): Long

    @Delete
    suspend fun delete(data: T): Int

    @Update
    suspend fun update(data: T): Int

    @Delete
    suspend fun deleteAll(data: List<T>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(data: List<T>): List<Long>
}